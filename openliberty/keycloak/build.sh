#!/bin/sh
IMAGE_NAME=openliberty
VERSION=v1-mongodb-kc
docker image rm pablobastidasv/${IMAGE_NAME}:${VERSION} || true
docker image build -t pablobastidasv/${IMAGE_NAME}:${VERSION} .
